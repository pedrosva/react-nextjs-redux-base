import React, { Component } from "react";

import { connect } from "react-redux";

import { addArticle } from "../../src/actions";


import Example from "../example";

class Example2Component extends Component {
  constructor() {
    super();
    this.state = {
    };

    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(event) {
    event.preventDefault();
    this.props.addArticle({ title: 'Teste3', id: 3 });
  }


  render() {
    return (
      <div>
        <div>
          <button type="button" onClick={this.handleChange}>Click Me!</button>
        </div>
        <div>
          <Example />
        </div>
      </div>

    );
  }
}


function mapStateToProps(state) {
  return {
    article: state.app.articles,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    addArticle: article => dispatch(addArticle(article))
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Example2Component);