import React from 'react'
import App, { Container } from 'next/app'

import { Provider } from "react-redux";
import WithReduxStore from "../src/store/WithRedux";
import { PersistGate } from 'redux-persist/integration/react'

// class Layout extends React.Component {
//     render() {
//         const { children } = this.props
//         return <div className='layout'>{children}</div>
//     }
// }

class MyApp extends App {

    render() {
        const { Component, pageProps, reduxStore } = this.props;

        return (
            <Container>
                    <Provider store={reduxStore}>
                        <PersistGate loading={null} persistor={reduxStore.__persistor}>
                            <Component {...pageProps} />
                        </PersistGate>
                    </Provider>
            </Container>
        )
    }
}

export default WithReduxStore(MyApp)
