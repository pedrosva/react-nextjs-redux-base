import React, { PureComponent } from "react";
import { persistStore, persistReducer } from "redux-persist";
import ConfigureStore from "./ConfigureStore";
import reducer from "../reducers";

const isServer = typeof window === "undefined";
const __NEXT_REDUX_STORE__ = "__NEXT_REDUX_STORE__";

function getOrCreateStore(initialState) {
  // Always make a new store if server, otherwise state is shared between requests
  if (isServer) {
    return ConfigureStore(reducer, initialState);
  } else {
    // Create store if unavailable on the client and set it on the window object
    const storage = require("redux-persist/lib/storage").default;

    const persistConfig = {
      key: "nextjs",
      //blacklist: ['app'], // app will not be persisted
      //whitelist: ["fromClient"], // make sure it does not clash with server keys
      storage
    };

    const persistedReducer = persistReducer(persistConfig, reducer);
    const store = ConfigureStore(persistedReducer, initialState);

    store.__persistor = persistStore(store); //hack

    if (!window[__NEXT_REDUX_STORE__]) {
      window[__NEXT_REDUX_STORE__] = store;
    }
    return window[__NEXT_REDUX_STORE__];
  }
}

export default App => {
  return class AppWithRedux extends PureComponent {
    static async getInitialProps(appContext) {
      // Get or Create the store with `undefined` as initialState
      // This allows you to set a custom default initialState
      const reduxStore = getOrCreateStore();

      // Provide the store to getInitialProps of pages
      appContext.ctx.reduxStore = reduxStore;

      let appProps = {};
      if (typeof App.getInitialProps === "function") {
        appProps = await App.getInitialProps.call(App, appContext);
      }

      return {
        ...appProps,
        initialReduxState: reduxStore.getState()
      };
    }

    constructor(props) {
      super(props);
      this.reduxStore = getOrCreateStore(props.initialReduxState);
    }

    render() {
      return <App {...this.props} reduxStore={this.reduxStore} />;
    }
  };
};
