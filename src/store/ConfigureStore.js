// import { createStore, applyMiddleware } from "redux";
// import reducer from "../reducers/index";

// import thunk from "redux-thunk";

// import logger from "redux-logger";

// const store = createStore(
//     reducer,
//     applyMiddleware(thunk, logger)
// );

// export default store;

// export default function configureStore() {

//     return createStore(reducer, applyMiddleware(thunk, logger));
// }

import { applyMiddleware, createStore } from "redux";
import thunk from "redux-thunk";

import logger from "redux-logger";

function ConfigureStore(reducer, initialState = {}) {
   
    const store = createStore(
      reducer,
      initialState,
      applyMiddleware(thunk, logger)
    );
    return store;
  }
  
  export default ConfigureStore;