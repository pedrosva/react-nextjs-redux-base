// src/js/actions/index.js
import { ADD_ARTICLE } from "../constants/action-types";

// export function addArticle(payload) {
//     return { type: ADD_ARTICLE, payload };
// }

export const addArticle = payload => {
    return (dispatch, getState) => {
        return dispatch({ type: ADD_ARTICLE, payload });
    }
}
